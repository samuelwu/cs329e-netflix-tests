#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, netflix_rmse, netflix_remove_colon
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        a = netflix_eval(r, w)
        assert a < 1.00
        self.assertEqual(a, 0.96)

    def test_eval_2(self):
        r = StringIO("11518:\n54272\n1923441\n191832\n1724476\n1173761\n")
        w = StringIO()
        a = netflix_eval(r, w)
        assert a < 1.00
        self.assertEqual(a, 0.94)

    def test_eval_3(self):
        r = StringIO("1152:\n340431\n1689219\n2494707\n1222218\n1619734\n905570\n")
        w = StringIO()
        a = netflix_eval(r, w)
        assert a < 1.00
        self.assertEqual(a, 0.81)

    # ----
    # rmse
    # ----

    def test_rmse_1(self):
        a = netflix_rmse([2, 3, 4], [5, 6, 7])
        self.assertEqual(a, 3.00)

    def test_rmse_2(self):
        a = netflix_rmse([0, 0, 0], [0, 0, 0])
        self.assertEqual(a, 0.00)

    def test_rmse_3(self):
        a = netflix_rmse([-2, -3, -4], [2, 3, 4])
        self.assertEqual(a, 6.22)

    def test_rmse_4(self):
        a = netflix_rmse([2, 3, 4], [-2, -3, -4])
        self.assertEqual(a, 6.22)

    def test_rmse_5(self):
        a = netflix_rmse([-1, -2, -3], [-4, -5, -6])
        self.assertEqual(a, 3.00)

    # ----------
    # remove_colon
    # ----------

    def test_removecolon_1(self):
        a = netflix_remove_colon("hello:")
        self.assertEqual(a, "hello")

    def test_removecolon_2(self):
        a = netflix_remove_colon("10040:")
        self.assertEqual(a, "10040")

    def test_removecolon_3(self):
        a = netflix_remove_colon("hello10040:")
        self.assertEqual(a, "hello10040")


# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
